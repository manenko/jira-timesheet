(function() {
    var shell = require('electron').shell;
    // open links externally by default
    jQuery(document).on('click', 'a[href^="http"]', function(event) {
        event.preventDefault();
        shell.openExternal(this.href);
    });
})();
