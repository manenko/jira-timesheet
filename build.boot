(set-env!
 :source-paths   #{"src" "scripts"}
 :resource-paths #{"resources"}
 :dependencies   '[[org.clojure/clojure "1.8.0" :scope "provided"]
                   [org.clojure/clojurescript "1.9.229"]
                   [org.clojure/core.async "0.2.385" :exclusions [org.clojure/tools.reader]]
                   [reagent "0.5.1"]
                   [cljs-ajax "0.5.8"]
                   [com.taoensso/timbre "4.7.4"]
                   [com.andrewmcveigh/cljs-time "0.4.0"]
                   [adzerk/boot-cljs "1.7.228-1" :scope "test"]
                   [adzerk/boot-cljs-repl "0.3.2" :scope "test"]
                   [adzerk/boot-reload "0.4.12" :scope "test"]
                   [com.cemerick/piggieback "0.2.1" :scope "test"]
                   [weasel "0.7.0" :scope "test"]
                   [org.clojure/tools.nrepl "0.2.12" :scope "test"]])

(require
 '[adzerk.boot-cljs      :refer [cljs]]
 '[adzerk.boot-cljs-repl :refer [cljs-repl
                                 start-repl]]
 '[adzerk.boot-reload    :refer [reload]]
 '[boot.task.built-in    :refer [sift]]
 '[clojure.java.io       :as    io])

(deftask dev-build []
  (comp (cljs-repl :ids #{"jira-timesheet"})
        (reload    :ids #{"jira-timesheet"}
                   :ws-host "localhost"
                   :on-jsload 'jira-timesheet.core/init)
        (cljs      :ids #{"jira-timesheet"})
        (cljs      :ids #{"main"}
                   :compiler-options {:asset-path "target/main.out"
                                      :closure-defines {'jira-timesheet.main/dev? true}})))

(deftask release-build []
  (comp (cljs :ids #{"main"}
              :optimizations :simple)
        (cljs :ids #{"jira-timesheet"}
              :optimizations :advanced)
        (sift
         :include #{#"\.out" #"\.cljs\.edn"}
         :invert true
         :move {#"(.+$)" "app/$1"})))

(defn- generate-lein-project-file!
  [& {:keys [keep-project] :or {:keep-project true}}]
  (let [pfile (io/file "project.clj")
        pname (or (get-env :project) 'boot-project)
        pvers (or (get-env :version) "0.1.0-SNAPSHOT")
        prop #(when-let [x (get-env %2)] [%1 x])
        head (list* 'defproject pname pvers
                    (concat
                     (prop :url :url)
                     (prop :license :license)
                     (prop :description :description)
                     [:dependencies (get-env :dependencies)
                      :source-paths (vec (get-env :source-paths))]))
        proj (pp-str (concat head (mapcat identity (get-env :lein))))]
    (if-not keep-project (.deleteOnExit pfile))
    (spit pfile proj)))

(deftask lein-generate
  "Generate a leiningen `project.clj` file.
  This task generates a leiningen `project.clj` file based on the boot
  environment configuration, including project name and version (generated
  if not present), dependencies, and source paths. Additional keys may be added
  to the generated `project.clj` file by specifying a `:lein` key in the boot
  environment whose value is a map of keys-value pairs to add to `project.clj`."
  [] (generate-lein-project-file! :keep-project true))

