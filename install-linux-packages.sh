#!/bin/sh

sudo apt-get install --no-install-recommends -y rpm
sudo apt-get install --no-install-recommends -y icnsutils graphicsmagick xz-utils
sudo add-apt-repository ppa:ubuntu-wine/ppa -y
sudo apt-get update
sudo apt-get install --no-install-recommends -y wine1.8
