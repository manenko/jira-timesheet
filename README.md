# README #


### What is jira-timesheet? ###

As of May 6, the JIRA Timesheet Reports and Gadgets plugin has been removed from JIRA Cloud. There are alternative addons that can be used as a replacement, but most of them are not free.

I developed **jira-timesheet** report as an alternative solution. This is a simple application with a single purpose: provide a timesheet reports. You can get the idea from the following screenshot:

![Screenshot of jira-timesheet application](doc/img/screenshot.png)


### Where I can download the app? ###

You can download latest version of JIRA timesheet application here:

https://www.dropbox.com/sh/ba06f8xx05iugxh/AADaBt3AsZ96jwtanX0P0p9oa?dl=0

### How to build it? ###

1. Install `node`, `npm`.
2. To package the application you should install (on Ubuntu just run `./install-linux-packages.sh`):
    1. icnsutils
    2. graphicsmagick
    3. xz-utils
    4. wine1.8
3. Open terminal and `cd` to the root of the repository.
4. Run `npm install` to install node dependencies.
5. Development build:
    1. `./boot watch dev-build`.
    2. In separate terminal: `./electron-dev.sh`.
    3. Edit the code, save, application will reload automatically.
6. Package application
    1. `./boot release-build && node_modules/.bin/build -lmw`. You should have all dependencies I mentioned in 2.1-2.4 installed.

### Work is in progress ###



### LICENSE ###


```
Copyright 2016-2017 Oleksandr Manenko

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
