(ns jira-timesheet.localstorage)

(defn set-item!
  "Set `key` in browser's local storage to `val`."
  [key val]
  (.setItem (.-localStorage js/window) key val))

(defn get-item
  "Returns value of `key` from browser's local storage."
  [key]
  (.getItem (.-localStorage js/window) key))

(defn remove-item!
  "Remove the browser's local storage value for the given `key`"
  [key]
  (.removeItem (.-localStorage js/window) key))

(defn clear!
  []
  "Clears local storage by emptying all keys out of storage."
  (.clear (.-localStorage js/window)))
