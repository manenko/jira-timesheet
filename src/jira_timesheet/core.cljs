(ns jira-timesheet.core
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [reagent.core :as reagent]
            [jira-timesheet.ui :as ui]
            [jira-timesheet.state :as state]
            [jira-timesheet.jira :as jira]
            [jira-timesheet.date :refer [to-string seconds->hours->str]]
            [cljs.core.async :refer [<!]]
            [cljs-time.coerce :refer [to-date-time]]
            [taoensso.timbre :refer-macros [info debug spy]]))

(defn error-alert
  []
  (if (nil? @state/error-state)
    [:span]
    [:div {:class "alert alert-danger"
           :role "alert"}
     (get @state/error-state :message "")]))

(defn info-alert
  []
  (if (nil? @state/info-state)
    [:span]
    [:div {:class "alert alert-info"
           :role "alert"}
     (get @state/info-state :message "")]))

(defn warning-alert
  []
  (if (nil? @state/warning-state)
    [:span]
    [:div {:class "alert alert-warning"
           :role "alert"}
     (get @state/warning-state :message "")]))

(defn account-settings
  []
  (let [{:keys [username password jira-url]} @state/account-state]
    [ui/panel "Account Settings"
     ^{:key "account-settings-form"}
     [:form
      [ui/url-field "Jira URL" "jira-url" jira-url
       {:input {:on-change #(state/update-jira-url! (ui/target-value %))}}]
      [ui/text-field "User Name" "username" username
       {:input {:on-change #(state/update-username! (ui/target-value %))}}]
      [ui/password-field "Password" "password" password
       {:input {:on-change #(state/update-password! (ui/target-value %))}}]]
     ^{:key "account-settings-save-button"}
     [ui/button-primary "Save" {:on-click #(state/save-app-state!)}]]))

(defn date-range-selector
  []
  [:div
   [ui/date-field "Start Date" "start-date-field" (to-string (:start @state/date-range-state))
    {:input {:on-change #(swap! state/date-range-state assoc :start (to-date-time (ui/target-value %)))}}]
   [ui/date-field "End Date" "end-date-field" (to-string (:end @state/date-range-state))
    {:input {:on-change #(swap! state/date-range-state assoc :end (to-date-time (ui/target-value %)))}}]])

(defn timesheet-button
  [username]
  [:button {:class "btn btn-primary"
            :type "button"
            :on-click (fn [e]
                        (state/reset-all-messages!)
                        (let [date-start (:start @state/date-range-state)
                              date-end (:end @state/date-range-state)]
                          (debug "timesheet-button :on-click " date-start)
                          (debug "timesheet-button :on-click " date-end)
                          (if (or (nil? date-start)
                                  (nil? date-end))
                            (state/update-error-message! "Please, select start date and end date.")
                            (state/refresh-timesheet! date-start date-end username))))}
   "Build Timesheet Report"])

(defn ^:private issue-page-url
  [issue-id]
  (jira/issue-page-url (:jira-url @state/account-state) issue-id))

(defn ^:private key-summary
  [issue]
  [:a
   {:href (issue-page-url (state/get-or-na issue :key))}
   (str (state/get-or-na issue :key) ": " (state/get-or-na issue :summary))])

(defn ^:pirvate unique-key
  [issue]
  (let [{:keys [id key summary date comment]} issue]
    (str id "-$-" key "-$-" summary "-$-" date "-$-" comment)))

(defn activity-row
  [task activity]
  [:tr
   [:td (to-string (:date activity))]
   [:td (key-summary task)]
   [:td (seconds->hours->str (:time-spent activity))]])

(defn issue-row
  [issue]
  [[:tr {:key (str "header-" (unique-key issue))}
    [:td {:col-span "3"}
     [:h4.text-info (key-summary issue)]]]
   (for [task (:tasks issue)]
     (for [activity (:worklog task)]
       ^{:key (:id activity)}
       [activity-row task activity]))
   [:tr.active {:key (str "total-" (unique-key issue))}
    [:td {:col-span "2" :style {:text-align "right"}} [:strong "Total"]]
    [:td [:strong (seconds->hours->str (:time-spent issue))]]]])

(defn timesheet-report
  [username]
  [ui/panel "Timesheet Report"

   ^{:key "date-range-selector"}    [date-range-selector]
   ^{:key "build-timesheet-button"} [timesheet-button username]

   ^{:key "timesheet-report-container"}
   [:div [:br]
    ^{:key "info-alert"}    [info-alert]
    ^{:key "warning-alert"} [warning-alert]
    ^{:key "error-alert"}   [error-alert]
    
    (let [state @state/timesheet-state]
      (if (seq state)
        ^{:key "timesheet-table"}
        [:table {:class "table table-bordered"}
         [:thead
          [:tr
           [:th "Date"] [:th "Task"] [:th "Time Spent (hours)"]]]
         [:tbody
          (mapcat issue-row state)
          [:tr.info
           [:td {:col-span "2" :style {:text-align "right"}}
            [:h4 "Total for the given time period"]]
           [:td [:h4 (seconds->hours->str (reduce + (map :time-spent state)))]]]]]))]])

(defn app
  []
  [:div
   [account-settings]
   [timesheet-report (:username @state/account-state)]])

(defn init []
  (state/load-app-state!)
  (reagent/render-component [app]
                            (. js/document (getElementById "app-host"))))
