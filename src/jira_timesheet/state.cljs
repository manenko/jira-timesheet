(ns jira-timesheet.state
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [reagent.core :as reagent]
            [cljs.reader :as reader]
            [jira-timesheet.localstorage :as storage]
            [jira-timesheet.jira :as jira]
            [cljs.core.async :refer [<! alts!]]
            [clojure.string :as str]
            [cljs-time.core :refer [now within? interval]]
            [cljs-time.coerce :refer [to-date-time]]
            [cljs-time.format :refer [parse formatter]]
            [taoensso.timbre :refer-macros [info debug spy]]))

(defonce ^:private app-state
  (reagent/atom {:date-range {:start (now)
                              :end   (now)}
                 :account {:jira-url nil
                           :username nil
                           :password nil}
                 :error nil
                 :info nil
                 :warning nil}))

(def account-state
  (reagent/cursor app-state [:account]))

(def error-state
  (reagent/cursor app-state [:error]))

(def info-state
  (reagent/cursor app-state [:info]))

(def warning-state
  (reagent/cursor app-state [:warning]))

(def timesheet-state
  (reagent/cursor app-state [:timesheet]))

(def date-range-state
  (reagent/cursor app-state [:date-range]))

(defn update-jira-url!
  [url]
  (swap! account-state assoc :jira-url url))

(defn update-username!
  [username]
  (swap! account-state assoc :username username))

(defn update-password!
  [password]
  (swap! account-state assoc :password password))

(defn update-account!
  [account]
  (reset! account-state account))

(defn update-error-message!
  [message]
  (swap! error-state assoc :message message))

(defn update-error!
  [error]
  (reset! error-state error))

(defn reset-error!
  []
  (update-error! nil))

(defn update-info-message!
  [message]
  (swap! info-state assoc :message message))

(defn update-info!
  [info]
  (reset! info-state info))

(defn reset-info!
  []
  (update-info! nil))

(defn update-warning-message!
  [message]
  (swap! warning-state assoc :message message))

(defn update-warning!
  [warning]
  (reset! warning-state warning))

(defn reset-warning!
  []
  (update-warning! nil))

(defn reset-all-messages!
  []
  (reset-error!)
  (reset-warning!)
  (reset-info!))

(defn update-timesheet!
  ([timesheet]
   (reset! timesheet-state timesheet)
   (if-not (seq timesheet)
     (update-warning-message! "There is no any worklog record for the given time period."))))

(defn reset-timesheet!
  []
  (reset! timesheet-state nil))

(defn na-if-blank
  [v]
  (if (str/blank? v) "n/a" v))

(defn get-or-na
  [m k]
  (let [v (get m k "n/a")]
    (na-if-blank v)))

(defn get-in-or-na
  [m ks]
  (na-if-blank (get-in m ks)))

(def ^:private jira-time-formatter (formatter "yyyy-MM-dd'T'HH:mm:ss.SSSZ"))

(defn ^:private parse-jira-date
  [s]
  (parse jira-time-formatter s))

(defn ^:private build-activity
  [item]
  {:id (:id item)
   :time-spent (:timeSpentSeconds item)
   :comment (get-or-na item :comment)
   :date (parse-jira-date (:started item))})

(defn ^:private build-worklog-items
  [worklog-items]
  (map build-activity worklog-items))

(defn ^:private select-worklog-items-by-author
  [author items]
  (filter (fn [item]
            (= (get-in item [:author :name]) author))
          items))

(defn ^:private select-worklog-items-by-date
  [date-from date-to items]
  (filter (fn [item]
            (within? (interval date-from date-to)
                     (parse-jira-date (:started item))))
          items))

(defn ^:private build-task
  [author data date-from date-to]
  {:id (:id data)
   :key (:key data)
   :summary (get-in-or-na data [:fields :summary])
   :worklog (build-worklog-items
             (select-worklog-items-by-date date-from date-to
                                           (select-worklog-items-by-author
                                            author
                                            (get-in data [:fields :worklog :worklogs]))))})

(defn ^:private build-tasks
  [author data date-from date-to]
  (map #(build-task author % date-from date-to) data))

(defn ^:private build-issue
  [author parent tasks date-from date-to]
  {:id (:id parent)
   :key (:key parent)
   :summary (get-in-or-na parent [:fields :summary])
   :tasks (build-tasks author tasks date-from date-to)})

(defn ^:private build-issues
  [author date-from date-to data]
  (map (fn [[parent tasks]]
         (build-issue author parent tasks date-from date-to))
       data))

(defn ^:private build-timesheet
  [author jira-response date-from date-to]
  (->> (:issues jira-response)
       (group-by (fn [issue] (get-in issue [:fields :parent])))
       (build-issues author date-from date-to)))

(defn ^:private time-spent-on-activity
  [activity]
  (:time-spent activity))

(defn ^:private time-spent-on-task
  [task]
  (reduce + (map time-spent-on-activity (:worklog task))))

(defn ^:private time-spent-on-issue
  [issue]
  (reduce + (map time-spent-on-task (:tasks issue))))

(defn ^:private update-time-spent
  [timesheet]
  (map (fn [issue]
         (assoc issue :time-spent (time-spent-on-issue issue)))
       timesheet))

(defn ^:private error->str
  [error-info]
  (case (:id error-info)
    :authentication-failed "Authentication failed. Please, check your credentials."
    (str "Unknown error (" (:id error-info) "). Technical details: " (:details error-info))))

(defn refresh-timesheet!
  ([date-from date-to]
   (refresh-timesheet! date-from date-to (:username @account-state)))
  ([date-from date-to author]
   (debug "refresh-timesheet! Date-From " date-from)
   (debug "refresh-timesheet! Date-To " date-to)
   (reset-all-messages!)
   (reset-timesheet!)
   (update-info-message! "Requesting timesheet data...")
   (go
     (let [[resp-ch err-ch] (jira/fetch-worklog @account-state
                                                date-from
                                                date-to
                                                author)
           [response ch] (alts! [resp-ch err-ch])]
       (reset-info!)
       (cond
         (= ch resp-ch) (update-timesheet!
                          (update-time-spent
                            (build-timesheet author response date-from date-to)))
         (= ch err-ch) (update-error-message! (str/join (map error->str response))))))))

(defn load-app-state!
  "Loads previously stored part of the application state from the local storage."
  []
  (if-let [account (storage/get-item "account")]
    (update-account! (reader/read-string account))))

(defn save-app-state!
  "Stores part of the application state into the local storage."
  []
  (storage/set-item! "account" @account-state))
