(ns jira-timesheet.date
  (:require [cljs.pprint :as pprint]
            [cljs-time.format :as time-fmt]))

(defn to-string
  "Returns a string representation of obj in UTC time-zone
  using \"yyyy-MM-dd\" date-time representation."
  [obj]
  (some->> obj
           (time-fmt/unparse (:date time-fmt/formatters))))

(defn seconds->hours->str
  "Converts `seconds` to hours and then converts hours to string."
  [seconds]
  (pprint/cl-format nil "~,2f" (/ seconds 3600)))
