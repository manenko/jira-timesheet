(ns jira-timesheet.ui)

(defn target-value
  [e]
  (-> e .-target .-value))

(defn panel
  [title & body]
  [:div {:class "panel panel-default"}
   [:div.panel-heading
    [:h3.panel-title] title]
   [:div.panel-body body]])

(defn ^:private form-field
  [label-title input-type input-id input-value & [opts]]
  [:div.form-group
   [:label (merge {:for input-id} (:label opts)) label-title]
   [:input.form-control (merge {:id input-id
                                :value input-value
                                :type input-type}
                               (:input opts))]])

(defn url-field
  [title id url & [opts]]
  [form-field title "url" id url opts])

(defn text-field
  [title id text & [opts]]
  [form-field title "text" id text opts])

(defn password-field
  [title id password & [opts]]
  [form-field title "password" id password opts])

(defn date-field
  [title id date & [opts]]
  [form-field title "date" id date (merge {:data-date-format "yyyy/mm/dd"} opts)])

(defn ^:private button
  [text button-category & [opts]]
  [:button (merge {:class (str "btn btn-" button-category)
                   :type "button"}
                  opts)
   text])

(defn button-default
  [text & [opts]]
  [button text "default" opts])

(defn button-primary
  [text & [opts]]
  [button text "primary" opts])

(defn get-by-id
  [id]
  (.getElementById js/document id))
