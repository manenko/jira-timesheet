(ns jira-timesheet.jira
  (:require [goog.crypt.base64 :as base64]
            [jira-timesheet.error :as error]
            [clojure.string :as str]
            [ajax.core :as ajax]
            [cljs.core.async :as async]
            [jira-timesheet.date :refer [to-string]]
            [clojure.walk :refer [keywordize-keys]]))

(defn ^:private auth-header
  [username password]
  (str "Basic " (base64/encodeString (str username ":" password))))

(defn ^:private absolute-url?
  [url]
  (or (str/starts-with? url "http://")
      (str/starts-with? url "https://")))

(defn ^:private absolutize-url
  [jira-url url]
  (if (absolute-url? url)
    url
    (str jira-url "/rest/api/2" url)))

;; TODO: Maybe define a macro that defines an error and adds it to the global list automatically?
(defn ^:private authentication-failed
  [response]
  (let [login-reason (.getResponseHeader response "X-Seraph-LoginReason")]
    (if (and (seq login-reason)
             (str/includes? login-reason "AUTHENTICATED_FAILED"))
      (error/ErrorInfo. :authentication-failed response))))

(defn ^:private jira-errors
  [response]
  (->> [authentication-failed] ;; seq of functions to invoke on response to collect errors
       (map #(% response))
       (remove nil?)))

(defn ^:private jsonify-response
  [response]
  (keywordize-keys (js->clj (.getResponseJson response))))

(defn ^:private ajax-handler
  [response-channel error-channel]
  (fn [response]
    (let [errors (jira-errors response)]
      (if (empty? errors)
        (async/put! response-channel (jsonify-response response))
        (async/put! error-channel errors)))))

(defn ^:private ajax-error-handler
  [error-channel]
  (fn [error]
    (.debug js/console "ajax-error-handler" (clj->js error))
    (let [error
          (case (:status error)
            401 (error/ErrorInfo. :authentication-failed nil)
            ;; TODO: When I enter incorrect jira url, then error is:
            ;;       {:status 0, :status-text "Request failed.", :failure :failed}
            ;;       Handle this case.
            (error/ErrorInfo. :unhandled-error error))]
      (async/put! error-channel [error]))))

(defn ^:private GET
  [{:keys [username password jira-url]} url & [opts]]
  (let [request-url      (absolutize-url jira-url url)
        response-channel (async/chan)
        error-channel    (async/chan)]
    (ajax/GET request-url
              ;; Unfortunately, there is no access to response's headers,
              ;; unless `:response-format` is `raw`. This is a 'by design' choice of
              ;; the author of `cljs-ajax` library. We have to parse the response manually.
              (merge {:response-format {:read identity :description "raw"}
                      :headers         {"Authorization" (auth-header username password)}
                      :handler         (ajax-handler response-channel error-channel)
                      :error-handler   (ajax-error-handler error-channel)}
                     opts))
    [response-channel error-channel]))

(defn ^:private jql-worklog-query
  "Builds JQL query that requests worklog for specified date range and author."
  [date-from date-to author]
  (str "worklogDate >= \""        (to-string date-from)
       "\" and worklogDate <= \"" (to-string date-to)
       "\" and worklogAuthor = "  author
       "&fields=timespent,issuetype,project,parent,summary,created,updated,worklog"))

(defn ^:private jql-query-url
  "Builds relative URL for specified JQL query."
  ([jql]
   (str "/search?jql=" (js/encodeURI jql))))

(defn fetch-worklog
  "Fetches JIRA issues which worklog items were created by
  specified `author` and fall within specified date range.

  Return core async channel that should be used to get the data."
  [account date-from date-to author]
  (GET account (jql-query-url (jql-worklog-query date-from date-to author))))

(defn issue-page-url
  [jira-url issue-id]
  (str jira-url "/browse/" issue-id))

(defn test-connection
  [account]
  (GET account "/myself"))
